<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\ActivityCollection;
use App\Http\Resources\ActivityResource;
use App\Models\Activity;
use App\Jobs\SendEmail;
use App\Mail\CampaignMail;
use App\Events\CampaignProcessed;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
    	return new ActivityCollection(Activity::orderBy('created_at', 'DESC')->paginate(25));
    }

    public function show(Request $request)
    {
    	return new ActivityResource(Activity::findOrFail($request->id));
    }

    public function showRecipient(Request $request)
    {
    	return new ActivityCollection(Activity::whereRecipient($request->email)->get());
    }

    public function showSender(Request $request)
    {
        return new ActivityCollection(Activity::whereSender($request->email)->get());
    }

    public function search(Request $request)
    {
        return new ActivityCollection(
            Activity::where('sender', 'LIKE', '%'.$request->query('query').'%')
                ->orWhere('recipient', 'LIKE', '%'.$request->query('query').'%')
                ->orWhere('subject', 'LIKE', '%'.$request->query('query').'%')
                ->orderBy('created_at', 'DESC')
                ->paginate(25)
        );
    }

    public function store(Request $request)
    {
        $request->validate([
            'recipient' => 'required',
            'sender' => 'required',
            'subject' => 'required',
        ]);

    	$activity = $this->storeActivity($request);

        try {
            SendEmail::dispatch($activity, new CampaignMail($activity));
            CampaignProcessed::dispatch($activity);
        } catch (\Exception $e) {
            $activity->update(['status' => 'failed']);
        }

        return new ActivityResource($activity);
    }

    public function attachmentUpload(Request $request)
    {
        $fileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('uploads'), $fileName);

        return response()->json(['success'=>'You have successfully upload file.']);
    }

    protected function storeActivity(Request $request)
    {
        $activity = new Activity();
        $activity->sender = $request->sender;
        $activity->recipient = $request->recipient;
        $activity->subject = $request->subject;
        $activity->status = 'posted';
        $activity->text_content = $request->text_content;
        $activity->html_content = $request->html_content;
        $activity->attachments = json_encode($request->attachments);
        $activity->save();

        return $activity;
    }
}
