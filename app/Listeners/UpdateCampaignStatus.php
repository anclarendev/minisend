<?php

namespace App\Listeners;

use App\Events\CampaignProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateCampaignStatus implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CampaignProcessed  $event
     * @return void
     */
    public function handle(CampaignProcessed $event)
    {
        $event->activity->update(['status' => 'sent']);
    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\CampaignProcessed  $event
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(CampaignProcessed $event, $exception)
    {
        $event->activity->update(['status' => 'failed']);
    }
}
