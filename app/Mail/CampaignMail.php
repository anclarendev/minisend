<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CampaignMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Details
     */
    protected $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->view('emails.campaign')
            ->text('emails.campaign_plain')
            ->subject($this->details['subject'])
            ->from($this->details['sender'], 'MiniSend')
            ->to($this->details['recipient'])
            ->with([
                'contentText' => $this->details['text_content'],
                'contentHtml' => $this->details['html_content'],
            ]);

        if(isset($this->details['attachments']))
        {
            foreach (json_decode($this->details['attachments']) as $key => $attachment)
            $mail->attach(public_path('uploads/' . $attachment), [
                'as' => $attachment
            ]);
        }

        return $mail;
    }
}
