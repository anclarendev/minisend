import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'

// Attaching
Vue.use(Vuex);
Vue.use(Router);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('quill-editor', require('./components/QuillEditorComponent.vue').default);

// Store data
import store from "./store/index";

// Routes
import Routes from './routes/index.js';

// Pages & Components
import App from './views/App.vue';

const app = new Vue({
    el: '#app',
    components: { App },
    router: Routes,
    store: store,
});

export default app;
