import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import ActivityList from '../views/ActivityList'
import ActivityView from '../views/ActivityView'
import ActivityRecipientView from '../views/ActivityRecipientView'
import ActivitySenderView from '../views/ActivitySenderView'
import ComposeMail from '../views/ComposeMail'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: ActivityList
        },
        {
            path: '/activity/view/:id',
            name: 'activity-view',
            component: ActivityView,
        },
        {
            path: '/activity/recipient/:email',
            name: 'activity-recipient-view',
            component: ActivityRecipientView,
        },
        {
            path: '/activity/sender/:email',
            name: 'activity-sender-view',
            component: ActivitySenderView,
        },
        {
            path: '/compose',
            name: 'compose-mail',
            component: ComposeMail,
        },
    ],
    scrollBehavior() {
        return {x: 0, y: 0}
    }
});

export default router;