import axios from 'axios'

let actions = {
    SEARCH_ACTIVITIES({commit}, query) {
        let params = {
            query
        };
        axios.get(`/api/activities/search`, {params})
            .then(res => {
                {
                    commit('SET_ACTIVITIES', res.data);
                }
            }).catch(err => {
                console.log(err);
            });
    },
    GET_ACTIVITIES({commit}) {
        axios.get('/api/activities')
            .then(res => {
                {
                    commit('SET_ACTIVITIES', res.data);
                }
            }).catch(err => {
                console.log(err);
            });
    }
}

export default actions;
