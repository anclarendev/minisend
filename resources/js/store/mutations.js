let mutations = {
    SET_ACTIVITIES(state, activities) {
        state.activities = activities
    }
};

export default mutations;
