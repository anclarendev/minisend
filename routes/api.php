<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace('')->group(function(){
    Route::get('activities', 'App\Http\Controllers\Api\ActivityController@index')->name('activity.index');
    Route::get('activities/search', 'App\Http\Controllers\Api\ActivityController@search')->name('activity.search');
    Route::post('activity/attachment-upload', 'App\Http\Controllers\Api\ActivityController@attachmentUpload')->name('activity.attachment.upload');
    Route::get('activity/recipient/{email}', 'App\Http\Controllers\Api\ActivityController@showRecipient')->name('activity.show.recipient');
    Route::get('activity/sender/{email}', 'App\Http\Controllers\Api\ActivityController@showSender')->name('activity.show.sender');
    Route::get('activity/{id}', 'App\Http\Controllers\Api\ActivityController@show')->name('activity.show');
    Route::post('activity/store', 'App\Http\Controllers\Api\ActivityController@store')->name('activity.store');
});
